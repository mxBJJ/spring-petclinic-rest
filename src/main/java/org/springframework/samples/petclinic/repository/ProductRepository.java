package org.springframework.samples.petclinic.repository;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Product;

import java.util.Collection;

public interface ProductRepository {

    Collection<Product> findAll() throws DataAccessException;

}
