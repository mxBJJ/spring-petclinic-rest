package org.springframework.samples.petclinic.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.samples.petclinic.rest.JacksonCustomPetDeserializer;
import org.springframework.samples.petclinic.rest.JacksonCustomPetSerializer;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "products")
public class Product extends NamedEntity implements Serializable {

    private Double price;
    private String name;
    private Integer quantity;

    public Product() {
    }


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
