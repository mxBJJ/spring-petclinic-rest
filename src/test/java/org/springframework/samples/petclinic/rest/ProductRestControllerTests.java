package org.springframework.samples.petclinic.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.samples.petclinic.model.Product;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.samples.petclinic.service.clinicService.ApplicationTestConfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= ApplicationTestConfig.class)
@WebAppConfiguration
public class ProductRestControllerTests {

    @Autowired
    private ProductRestController productRestController;

    @MockBean
    protected ClinicService clinicService;

    private MockMvc mockMvc;

    private List<Product> products;


    @Before
    public void initProducts(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(productRestController)
            .setControllerAdvice(new ExceptionControllerAdvice())
            .build();
        products = new ArrayList<Product>();

        Product product = new Product();
        product.setId(1);
        product.setName("Produto Teste");
        product.setPrice(35.0);
        product.setQuantity(5);

        products.add(product);


        product = new Product();
        product.setId(2);
        product.setName("Produto para teste");
        product.setPrice(85.0);
        product.setQuantity(2);

        products.add(product);

    }

    @Test
    public void testGetAllProductsSuccess() throws Exception {
        given(this.clinicService.findAllProducts()).willReturn(products);
        this.mockMvc.perform(get("/api/products/")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))
            .andExpect(jsonPath("$.[0].id").value(1))
            .andExpect(jsonPath("$.[0].name").value("Produto Teste"))
            .andExpect(jsonPath("$.[0].price").value(35.0))
            .andExpect(jsonPath("$.[0].quantity").value(5))
            .andExpect(jsonPath("$.[1].id").value(2))
            .andExpect(jsonPath("$.[1].name").value("Produto para teste"))
            .andExpect(jsonPath("$.[1].price").value(85.0))
            .andExpect(jsonPath("$.[1].quantity").value(2));

    }



}
